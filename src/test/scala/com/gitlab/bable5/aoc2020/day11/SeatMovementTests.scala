package com.gitlab.bable5.aoc2020.day11

import org.scalatest.flatspec.AnyFlatSpec

class SeatMovementTests extends AnyFlatSpec {

  "Parsing" should "Find Empty seats, occupied seats and floor" in {
    val row = parseRow(s"#L###LL.L#".stripMargin)
    assert(row == Seq(Occupied, Empty, Occupied, Occupied, Occupied, Empty, Empty, Floor, Empty, Occupied))
  }

  val seatingChart =
    s"""###
       |###
       |###""".stripMargin
      .split("\n").map(parseRow).toIndexedSeq

  "Counting Neighbors" should "count adjacent occupied seats" in {
    val neighbors = NeighborCounterPart1((1, 1), seatingChart)
    assert(neighbors == 8)
  }
  it should "handle left edge" in {
    val neighbors = NeighborCounterPart1((0, 1), seatingChart)
    assert(neighbors == 5)
  }
  it should "handle right edge" in {
    val neighbors = NeighborCounterPart1((2, 1), seatingChart)
    assert(neighbors == 5)
  }
  it should "handle top edge" in {
    val neighbors = NeighborCounterPart1((1, 0), seatingChart)
    assert(neighbors == 5)
  }
  it should "handle bottom edge" in {
    val neighbors = NeighborCounterPart1((1, 2), seatingChart)
    assert(neighbors == 5)
  }
  it should "handle top-left corner" in {
    val neighbors = NeighborCounterPart1((0, 0), seatingChart)
    assert(neighbors == 3)
  }

  it should "count floor as empty" in {
    val seatingChart2 =
      s""".LL
         |###
         |#.#""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    val neighbors = NeighborCounterPart1((1, 1), seatingChart2)
    assert(neighbors == 4)
  }

  "Seat transitions" should "change an empty seat with no neighbors to occupied" in {
    assert(TransitionRulesPart1(Empty, 0) == Occupied)
  }
  it should "not transition empty if it has any neighbors" in {
    for (i <- 1 to 8) {
      assert(TransitionRulesPart1(Empty, i) == Empty)
    }
  }
  it should "transition to empty if it occupied and has 4 or more neighbors" in {
    assert(TransitionRulesPart1(Occupied, 4) == Empty)
    for (i <- 4 to 8) {
      assert(TransitionRulesPart1(Occupied, i) == Empty)
    }
  }
  it should "never transition floor to empty or occupied" in {
    for (i <- 0 to 8) {
      assert(TransitionRulesPart1(Floor, i) == Floor)
    }
  }

  "Example 1" should "take a step" in {
    val initial =
      s"""L.LL.LL.LL
         |LLLLLLL.LL
         |L.L.L..L..
         |LLLL.LL.LL
         |L.LL.LL.LL
         |L.LLLLL.LL
         |..L.L.....
         |LLLLLLLLLL
         |L.LLLLLL.L
         |L.LLLLL.LL""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq
    val step1 =
      s"""#.##.##.##
         |#######.##
         |#.#.#..#..
         |####.##.##
         |#.##.##.##
         |#.#####.##
         |..#.#.....
         |##########
         |#.######.#
         |#.#####.##""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    assert(SimulateSeatingPart1(initial) == step1)
  }
  it should "take two steps" in {
    val initial =
      s"""L.LL.LL.LL
         |LLLLLLL.LL
         |L.L.L..L..
         |LLLL.LL.LL
         |L.LL.LL.LL
         |L.LLLLL.LL
         |..L.L.....
         |LLLLLLLLLL
         |L.LLLLLL.L
         |L.LLLLL.LL""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq
    val step1 =
      s"""#.LL.L#.##
         |#LLLLLL.L#
         |L.L.L..L..
         |#LLL.LL.L#
         |#.LL.LL.LL
         |#.LLLL#.##
         |..L.L.....
         |#LLLLLLLL#
         |#.LLLLLL.L
         |#.#LLLL.##""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    assert(SimulateSeatingPart1(SimulateSeatingPart1(initial)) == step1)
  }
  it should "Run to a fixpoint" in {
    val initial =
      s"""L.LL.LL.LL
         |LLLLLLL.LL
         |L.L.L..L..
         |LLLL.LL.LL
         |L.LL.LL.LL
         |L.LLLLL.LL
         |..L.L.....
         |LLLLLLLLLL
         |L.LLLLLL.L
         |L.LLLLL.LL""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq
    val finalState =
      s"""#.#L.L#.##
         |#LLL#LL.L#
         |L.#.L..#..
         |#L##.##.L#
         |#.#L.LL.LL
         |#.#L#L#.##
         |..L.L.....
         |#L#L##L#L#
         |#.LLLLLL.L
         |#.#L#L#.##""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq
    assert(
      SimulateSeatingPart1.runToFixpoint(initial) == finalState
    )
  }
  it should "find 37 occupied seats when done" in {
    val finalState =
      s"""#.#L.L#.##
         |#LLL#LL.L#
         |L.#.L..#..
         |#L##.##.L#
         |#.#L.LL.LL
         |#.#L#L#.##
         |..L.L.....
         |#L#L##L#L#
         |#.LLLLLL.L
         |#.#L#L#.##""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    assert(SimulateSeatingPart1.totalOccupiedSeats(finalState) == 37)
  }
}
