package com.gitlab.bable5.aoc2020.day11

import org.scalatest.flatspec.AnyFlatSpec

class SeatMovementTestsPart2 extends AnyFlatSpec {
  "Seat counting " should "use the first visible seat in each direction" in {
    val initial =
      s""".......#.
         |...#.....
         |.#.......
         |.........
         |..#L....#
         |....#....
         |.........
         |#........
         |...#.....""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    assert(NeighborsCounterPart2((4, 3), initial) == 8)
  }
  it should "Find no occupied seats when all diagonals, verticals and horizontals are empty" in {
    val initial =
      s""".##.##.
         |#.#.#.#
         |##...##
         |...L...
         |##...##
         |#.#.#.#
         |.##.##.""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    assert(NeighborsCounterPart2((3, 3), initial) == 0)
  }

  "Seat transitions" should "change an empty seat with no neighbors to occupied" in {
    assert(TransitionRulesPart1(Empty, 0) == Occupied)
  }
  it should "not transition empty if it has any neighbors" in {
    for (i <- 1 to 8) {
      assert(TransitionRulesPart1(Empty, i) == Empty)
    }
  }
  it should "transition to empty if it occupied and has 5 or more neighbors" in {
    assert(TransitionRulesPart1(Occupied, 5) == Empty)
    for (i <- 5 to 8) {
      assert(TransitionRulesPart1(Occupied, i) == Empty)
    }
  }
  it should "never transition floor to empty or occupied" in {
    for (i <- 0 to 8) {
      assert(TransitionRulesPart1(Floor, i) == Floor)
    }
  }

  "Example 2" should "take step" in {
    val initial =
      s"""L.LL.LL.LL
         |LLLLLLL.LL
         |L.L.L..L..
         |LLLL.LL.LL
         |L.LL.LL.LL
         |L.LLLLL.LL
         |..L.L.....
         |LLLLLLLLLL
         |L.LLLLLL.L
         |L.LLLLL.LL""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq
    val step1 =
      s"""#.##.##.##
         |#######.##
         |#.#.#..#..
         |####.##.##
         |#.##.##.##
         |#.#####.##
         |..#.#.....
         |##########
         |#.######.#
         |#.#####.##""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq
    assert(SimulateSeatingPart2(initial) == step1)
  }
  it should "Finish with 26 filled seats" in {
    val initial =
      s"""L.LL.LL.LL
         |LLLLLLL.LL
         |L.L.L..L..
         |LLLL.LL.LL
         |L.LL.LL.LL
         |L.LLLLL.LL
         |..L.L.....
         |LLLLLLLLLL
         |L.LLLLLL.L
         |L.LLLLL.LL""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    val finalState =
      s"""#.L#.L#.L#
         |#LLLLLL.LL
         |L.L.L..#..
         |##L#.#L.L#
         |L.L#.LL.L#
         |#.LLLL#.LL
         |..#.L.....
         |LLL###LLL#
         |#.LLLLL#.L
         |#.L#LL#.L#""".stripMargin
        .split("\n").map(parseRow).toIndexedSeq

    assert(
      SimulateSeatingPart2.totalOccupiedSeats(
        SimulateSeatingPart2.runToFixpoint(initial)
      ) == 26
    )
  }
}
