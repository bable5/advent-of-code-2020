package com.gitlab.bable5.aoc2020.day06

import org.scalatest.flatspec.AnyFlatSpec

class InputGrouperTest extends AnyFlatSpec {
  "Input example 1" should "create sets of inputs" in {
    val input = """abc
        |
        |a
        |b
        |c
        |
        |ab
        |ac
        |
        |a
        |a
        |a
        |a
        |
        |b
        |""".stripMargin

    val expected = Seq(
      List(Set("a", "b", "c")),
      List(Set("a"), Set("b"), Set("c")),
      List(Set("a", "b"), Set("a", "c")),
      List(Set("a"), Set("a"), Set("a"), Set("a")),
      List(Set("b"))
    )

    val actual = passportDataGrouper(input.split("\n"))

    assert(actual == expected)
  }
}
