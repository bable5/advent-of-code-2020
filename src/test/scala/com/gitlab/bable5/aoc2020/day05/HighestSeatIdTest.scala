package com.gitlab.bable5.aoc2020.day05

import org.scalatest.flatspec.AnyFlatSpec

class HighestSeatIdTest extends AnyFlatSpec {
  "Highest seat id example" should "be 820" in {
    val input = Seq(
      "BFFFBBFRRR",
      "FFFBBBFRRR",
      "BBFFBBFRLL"
    )

    assert(HighestSeatId(input) == 820)
  }
}
