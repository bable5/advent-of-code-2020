package com.gitlab.bable5.aoc2020.day05

import org.scalatest.flatspec.AnyFlatSpec

class SeatLocationParserTest extends AnyFlatSpec {

  "BFFFBBFRRR" should "be seat row 70, column 7, id 567" in {
    val input = "BFFFBBFRRR"
    val seat = SeatLocationParser(input)

    assert(seat.row == 70)
    assert(seat.column == 7)
    assert(seat.seatId == 567)
  }

  "FFFBBBFRRR" should "be seat row 70, column 7, id 567" in {
    val input = "FFFBBBFRRR"
    val seat = SeatLocationParser(input)

    assert(seat.row == 14)
    assert(seat.column == 7)
    assert(seat.seatId == 119)
  }

  "BBFFBBFRLL" should "be seat row 70, column 7, id 567" in {
    val input = "BBFFBBFRLL"
    val seat = SeatLocationParser(input)

    assert(seat.row == 102)
    assert(seat.column == 4)
    assert(seat.seatId == 820)
  }
}
