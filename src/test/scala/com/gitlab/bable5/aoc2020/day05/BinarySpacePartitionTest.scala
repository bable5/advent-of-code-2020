package com.gitlab.bable5.aoc2020.day05

import org.scalatest.flatspec.AnyFlatSpec

class BinarySpacePartitionTest extends AnyFlatSpec {

  "LULUUL with 0-127" should "be 44" in {
    val directions = Seq(LowerHalf, UpperHalf, LowerHalf, UpperHalf, UpperHalf, LowerHalf, LowerHalf)
    val range = BSPRange(0, 127)

    val finalRange = BinarySpacePartition(directions, range)

    assert(finalRange.min == 44)
    assert(finalRange.max == 44)
  }

  "ULU with 0-8" should "be 5" in {
    val directions = Seq(UpperHalf, LowerHalf, UpperHalf)
    val range = BSPRange(0, 7)

    val finalRange = BinarySpacePartition(directions, range)

    assert(finalRange.min == 5)
    assert(finalRange.max == 5)
  }

  "Split upper for 0-127" should "be 64-127" in {
    val directions = Seq(UpperHalf)
    val range = BSPRange(0, 127)

    val finalRange = BinarySpacePartition(directions, range)

    assert(finalRange.min == 64)
    assert(finalRange.max == 127)
  }

  "Split lower for 0-127" should "be 0-63" in {
    val directions = Seq(LowerHalf)
    val range = BSPRange(0, 127)

    val finalRange = BinarySpacePartition(directions, range)

    assert(finalRange.min == 0)
    assert(finalRange.max == 63)
  }
}
