package com.gitlab.bable5.aoc2020.day09

import com.gitlab.bable5.aoc2020.day09.EncodingCracker._
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec

class EncodingErrorTest extends AnyFlatSpec with GivenWhenThen {
  behavior of "Example 1"

  it should "find 127 as the first number that doesn't match" in {
    assert(
      EncodingCracker(5, example1Data) == 127
    )
  }

  it should "Find 127 doesn't match " in {
    assert(
      EncodingCracker(5, Seq(95, 102, 117, 150, 182, 127)) == 127
    )
  }

  it should "Check the first partition and find the sum" in {
    assert(somePairSumsTo(40, Seq(35, 20, 15, 25, 47)))
  }

  it should "Not find 127 in that example's window" in {
    assert(
      !somePairSumsTo(127, Seq(95, 102, 117, 150, 182))
    )
  }

  it should "check by partition size" in {
    assert(
      partition(5, Seq(35, 20, 15, 25, 47, 40, 62))
        == (Seq(35, 20, 15, 25, 47), Seq(40, 62))
    )
  }
  it should "allow any partition size" in {
    assert(
      partition(2, Seq(1, 2, 3, 4, 5)) == (Seq(1, 2), Seq(3, 4, 5))
    )
  }

  "Combining input" should "create pairwise elements from a list" in {
    val input = Seq(1, 2, 3, 4)
    assert(InputCombiner(input) ==
      Seq((1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4))
    )
  }

  "Cracking the code" should "find a contiguous subset that sums to 127" in {
    val subSeq = FindSubSequence(127, example1Data)

    assert(subSeq == Seq(15,25,47,40))
  }

  def example1Data: Seq[Long] = Seq(
    35,
    20,
    15,
    25,
    47,
    40,
    62,
    55,
    65,
    95,
    102,
    117,
    150,
    182,
    127,
    219,
    299,
    277,
    309,
    576
  )
}
