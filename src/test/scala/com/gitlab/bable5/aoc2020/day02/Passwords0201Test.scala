package com.gitlab.bable5.aoc2020.day02

import org.scalatest.flatspec.AnyFlatSpec

class Passwords0201Test extends AnyFlatSpec {
  "1-3 a: abcde" should "be valid" in {
    val input = PasswordCriteria(1, 3, 'a', "abcde")

    assert(input.isValid)
  }

  "1-3 b: cdefg" should "not be valid" in {
    val input = PasswordCriteria(1, 3, 'b', "cdefg")

    assert(!input.isValid)
  }

  "2-9 c: ccccccccc" should "be valid" in {
    val input = PasswordCriteria(2, 9, 'c', "ccccccccc")

    assert(input.isValid)
  }
}
