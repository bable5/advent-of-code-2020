package com.gitlab.bable5.aoc2020.day02

import org.scalatest.flatspec.AnyFlatSpec

class Parser0101Test extends AnyFlatSpec {
  "1-3 a: abcde" should "parse" in {
    val input = "1-3 a: abcde"
    val expected = PasswordCriteria(1, 3, 'a', "abcde")
    val acual = InputParser(input)

    assert(acual == expected)
  }

  "1-3 b: cdefg" should "parse" in {
    val input = "1-3 b: cdefg"
    val expected = PasswordCriteria(1, 3, 'b', "cdefg")
    val actual = InputParser(input)

    assert(actual == expected)
  }

  "2-9 c: ccccccccc" should "parse" in {
    val input = "2-9 c: ccccccccc"
    val expected = PasswordCriteria(2, 9, 'c', "ccccccccc")
    val actual = InputParser(input)

    assert(actual == expected)
  }

  "Trim leading and trailing whitespace" should "parse" in {
    val input = "   \t\n 2-9 c: ccccccccc  \n\n\n\t"
    val expected = PasswordCriteria(2, 9, 'c', "ccccccccc")
    val actual = InputParser(input)

    assert(actual == expected)
  }
}
