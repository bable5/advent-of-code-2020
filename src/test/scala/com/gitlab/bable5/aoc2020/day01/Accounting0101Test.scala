package com.gitlab.bable5.aoc2020.day01

import org.scalatest.flatspec.AnyFlatSpec

class Accounting0101Test extends AnyFlatSpec {

  val solver = new Accounting0101

  "It" should "find the pair in test scenario" in {
    val input: Iterable[Long] = Seq(1721, 979, 366, 299, 675, 1456)
    val target = 2020

    val pair = solver.find(target, input)

    assert(pair == Some(1721, 299))
  }

  "It" should "compare all sub-lists" in {
    val input: Iterable[Long] = Seq(979, 366, 1456, 299, 1721)
    val target = 2020

    val pair = solver.find(target, input)

    assert(pair == Some(299, 1721))
  }

  "Find 3 numbers" should "sum to the target" in {
    val input: Iterable[Long] = Seq(1721, 979, 366, 299, 675, 1456)
    val target = 2020

    val pair = solver.find3(target, input)

    assert(pair == Some(979, 366, 675))
  }
}
