package com.gitlab.bable5.aoc2020.day12

import org.scalatest.flatspec.AnyFlatSpec

class Day12Part1Tests extends AnyFlatSpec {
  private val example1: Seq[Move] = Seq(
    Forward -> 10,
    North -> 3,
    Forward -> 7,
    Right -> 90,
    Forward -> 11
  )

  private val testOrigin = (0, 0, 90)

  private def run(movements: Seq[Move], initialPosition: Position): Position = movements.foldLeft(initialPosition)(nextPosition)


  "example one" should "move to E17, S8" in {
    val (x, y, _) = run(example1, testOrigin)

    assert(x == 17)
    assert(y == -8)
  }

  "G"

  "Move north" should "add 1 to y" in {
    val (x, y, h) = run(Seq(North -> 1), (0, 0, 0))
    assert(x == 0)
    assert(y == 1)
    assert(h == 0)
  }


  "Move south" should "substract 1 from y" in {
    val (x, y, h) = run(Seq(South -> 1), (0, 0, 0))
    assert(x == 0)
    assert(y == -1)
    assert(h == 0)
  }


  "Move east" should "add 1 to x" in {
    val (x, y, h) = run(Seq(East -> 1), (0, 0, 0))
    assert(x == 1)
    assert(y == 0)
    assert(h == 0)
  }

  "Move west" should "subtract 1 from y" in {
    val (x, y, h) = run(Seq(West -> 1), (0, 0, 0))
    assert(x == -1)
    assert(y == 0)
    assert(h == 0)
  }

  "Right" should "add to the heading" in {
    val (x, y, h) = run(Seq(Right -> 90), (0, 0, 0))
    assert(x == 0)
    assert(y == 0)
    assert(h == 90)
  }

  "Left" should "subtract from the heading" in {
    val (x, y, h) = run(Seq(Left -> 90), (0, 0, 0))
    assert(x == 0)
    assert(y == 0)
    assert(h == -90)
  }

  "Forward" should "go north" in {
    val (x, y, h) = run(Seq(Forward -> 1), (0, 0, 0))
    assert(x == 0)
    assert(y == 1)
    assert(h == 0)
  }
  it should "go south" in {
    val (x, y, h) = run(Seq(Forward -> 1), (0, 0, 180))
    assert(x == 0)
    assert(y == -1)
    assert(h == 180)
  }
  it should "go east" in {
    val (x, y, h) = run(Seq(Forward -> 1), (0, 0, 90))
    assert(x == 1)
    assert(y == 0)
    assert(h == 90)
  }
  it should "go west" in {
    val (x, y, h) = run(Seq(Forward -> 1), (0, 0, 270))
    assert(x == -1)
    assert(y == 0)
    assert(h == 270)
  }
  it should "go west via negative heading" in {
    val (x, y, h) = run(Seq(Forward -> 1), (0, 0, -90))
    assert(x == -1)
    assert(y == 0)
    assert(h == -90)
  }

  "Parsing" should "convert to movements" in {
    assert(
    Seq("F10", "N3", "F7", "R90", "F11").map(parseMovement)
      == example1
    )
  }
}
