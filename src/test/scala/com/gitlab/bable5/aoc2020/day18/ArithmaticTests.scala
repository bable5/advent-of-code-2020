package com.gitlab.bable5.aoc2020.day18

import com.gitlab.bable5.aoc2020.{Parser, ParserArithPrecidenc}
import org.scalatest.flatspec.AnyFlatSpec

class ArithmaticTests extends AnyFlatSpec {
  behavior of "Parser"
  it should "parse a simple BinOp" in {
    val actual = parseTree(Parser.Expr, Seq("1", "+", "2"))
    val expected = BinOp("+", Terminal("1"), Terminal("2"))

    assert(actual == expected)
  }

  it should "parse a simple a sequence ofBinOp" in {
    val actual = parseTree(Parser.Expr, Seq("1", "+", "2", "*", "3"))
    val expected = BinOp("*", BinOp("+", Terminal("1"), Terminal("2")), Terminal("3"))

    assert(actual == expected)
  }

  it should "parse ()" in {
    val actual = parseTree(Parser.Expr, Seq("(", "1", "+", "2", ")"))
    val expected = BinOp("+", Terminal("1"), Terminal("2"))

    assert(actual == expected)
  }

  it should "parse () with higher precedence" in {
    val actual = parseTree(Parser.Expr, Seq("1", "+", "(", "2", "*", "3", ")"))
    val expected = BinOp("+", Terminal("1"), BinOp("*", Terminal("2"), Terminal("3")))

    assert(actual == expected)
  }


  behavior of "special arithmetic"
  it should "add 2 numbers" in {
    assert(eval(Seq("1", "+", "2")) == 3)
  }

  it should "multiply two numbers" in {
    assert(eval(Seq("2", "*", "3")) == 6)
  }

  it should "use left to right precedence for + and * " in {
    assert(eval(Seq("1", "+", "2", "*", "3")) == 9)
  }

  it should "'override' precedence with ()" in {
    assert(eval(Seq("1", "+", "(", "2", "*", "3", ")")) == 7)
  }

  def test(s: String, expected: Long): Unit = {
    val actual = eval(s.split("\\s"))
    assert(actual == expected)
  }

  behavior of "full examples"
  it should "2 * 3 + (4 * 5)" in {
    test("2 * 3 + (4 * 5)", 26)
  }

  it should "5 + (8 * 3 + 9 + 3 * 4 * 3)" in {
    test("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437)
  }

  it should "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))" in {
    test("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240)
  }

  it should "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" in {
    test("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632)
  }

  def testArithPrecidence(s: String, expected: Long): Unit = {
    val actual = evalArithPrecedence(s.split("\\s"))
    assert(actual == expected)
  }

  implicit def toTerminal(s: String): Terminal = Terminal(s)

  behavior of "Higher arith precidence"
  it should "parse a simple +" in {
    val actual = parseTree(ParserArithPrecidenc.Expr, "1 + 2".split("\\s"))
    val expected = BinOp("+", "1", "2")
    assert(actual == expected)
  }
  it should "parse a simple *" in {
    val actual = parseTree(ParserArithPrecidenc.Expr, "1 * 2".split("\\s"))
    val expected = BinOp("*", "1", "2")
    assert(actual == expected)
  }
  it should "parse with higher + precedence" in {
    val actual = parseTree(ParserArithPrecidenc.Expr, "1 * 2 + 3".split("\\s"))
    val expected = BinOp("*", "1", BinOp("+", "2", "3"))
    assert(actual == expected)
  }

  it should "eval example 1" in {
    testArithPrecidence("1 + (2 * 3) + (4 * (5 + 6))", 51)
  }
  it should "eval example 2" in {
    testArithPrecidence("2 * 3 + (4 * 5)", 46)
  }
  it should "eval example 5" in {
    testArithPrecidence("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23340)
  }
}
