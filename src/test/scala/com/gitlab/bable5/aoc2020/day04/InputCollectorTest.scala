package com.gitlab.bable5.aoc2020.day04

import org.scalatest.flatspec.AnyFlatSpec

class InputCollectorTest extends AnyFlatSpec with InputCollector {
  "collect" should "concat lines separated by a blank line" in {
    val input =
      s"""ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
         |byr:1937 iyr:2017 cid:147 hgt:183cm
         |
         |hcl:#ae17e1 iyr:2013
         |eyr:2024
         |ecl:brn pid:760753108 byr:1931
         |hgt:179cm
         |""".stripMargin

    val result = collectPassportEntry(input.split("\n").toIterable)
    val expected = Seq(
      List("ecl:gry", "pid:860033327", "eyr:2020", "hcl:#fffffd", "byr:1937", "iyr:2017", "cid:147", "hgt:183cm"),
      List("hcl:#ae17e1", "iyr:2013", "eyr:2024", "ecl:brn", "pid:760753108", "byr:1931", "hgt:179cm")
    )

    assert(result == expected)
  }

  "a multiline entry" should "parse as single list" in {
    val input = List("ecl:gry", "pid:860033327", "eyr:2020", "hcl:#fffffd", "byr:1937", "iyr:2017", "cid:147", "hgt:183cm")

    val expected: Seq[(String, String)] = Seq(("ecl","gry"), ("pid", "860033327"), ("eyr", "2020"),
      ("hcl", "#fffffd"), ("byr", "1937"), ("iyr", "2017"), ("cid", "147"), ("hgt", "183cm"))

    val actual = parsePassportEntry(input)

    assert(actual == expected)
  }
}
