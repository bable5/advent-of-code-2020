package com.gitlab.bable5.aoc2020.day04

import org.scalatest.flatspec.AnyFlatSpec

class Passport0401MainTest extends AnyFlatSpec with InputCollector {

  "AOC Day 4 Part 1 example" should "find 2 entries" in {
    val input =
      """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
        |byr:1937 iyr:2017 cid:147 hgt:183cm
        |
        |iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
        |hcl:#cfa07d byr:1929
        |
        |hcl:#ae17e1 iyr:2013
        |eyr:2024
        |ecl:brn pid:760753108 byr:1931
        |hgt:179cm
        |
        |hcl:#cfa07d eyr:2025 pid:166559648
        |iyr:2011 ecl:brn hgt:59in""".stripMargin

    val validCount = collectPassportEntry(input.split("\n"))
      .map(parsePassportEntry)
      .count(Validator.hasRequiredFields)

    assert(validCount == 2)
  }
}
