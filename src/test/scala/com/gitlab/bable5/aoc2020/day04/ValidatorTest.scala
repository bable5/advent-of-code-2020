package com.gitlab.bable5.aoc2020.day04

import org.scalatest.flatspec.AnyFlatSpec

class ValidatorTest extends AnyFlatSpec with InputCollector {

  "2002" should "byr valid" in {
    assert(Validator("byr", "2002"))
  }

  "2003" should "byr invalid" in {
    assert(!Validator("byr", "2003"))
  }

  "60in" should "be a valid hgt" in assert(Validator("hgt", "60in"))
  "190cm" should "be a valid hgt" in assert(Validator("hgt", "190cm"))
  "190in" should "be an invalid hgt" in assert(!Validator("hgt", "190in"))
  "190" should "be an invalid hgt" in assert(!Validator("hgt", "190"))

  "#123abc" should "be a valid hcl" in assert(Validator("hcl", "#123abc"))
  "#1234abc" should "be an invalid hcl" in assert(!Validator("hcl", "#1234abc"))
  "#123abz" should "be an invalid hcl" in assert(!Validator("hcl", "#123abz"))
  "123abc" should "be an invalid hcl" in assert(!Validator("hcl", "123abc"))


  "000000001" should "be a valid pid" in assert(Validator("pid", "000000001"))
  "0123456789" should "be an invalid pid" in assert(!Validator("pid", "0123456789"))

  "Passport example 1" should "be invalid" in
    assert(
      !Validator(parsePassportEntry(
        List("eyr:1972", "cid:100", "hcl:#18171d", "ecl:amb", "hgt:170", "pid:186cm", "iyr:2018", "byr:1926"))
      )
    )

  "Passport example 5" should "be valid" in
    assert(
      Validator(parsePassportEntry(List("pid:087499704", "hgt:74in", "ecl:grn", "iyr:2012", "eyr:2030", "byr:1980", "hcl:#623a2f")))
    )

}
