package com.gitlab.bable5.aoc2020.day18

import com.gitlab.bable5.aoc2020.Timer.timed

object Day1802Main extends App {

  println(timed {
    Day18Data().map(input =>
      evalArithPrecedence(input)
    ).sum
  })

}
