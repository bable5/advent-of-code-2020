package com.gitlab.bable5.aoc2020

import atto.Atto.{char, digit, many, ok, toParserOps}
import atto.{ParseResult, Parser}
import com.gitlab.bable5.aoc2020.day18.{BinOp, ParseTree, Terminal}

package object day18 {

  sealed trait ParseTree

  case class BinOp(op: String, lhs: ParseTree, rhs: ParseTree) extends ParseTree

  case class Terminal(op: String) extends ParseTree

  def traverse(parseTree: ParseTree): Long = parseTree match {
    case BinOp(op, lhs, rhs) => op match {
      case "+" => traverse(lhs) + traverse(rhs)
      case "*" => traverse(lhs) * traverse(rhs)
    }
    case Terminal(op) => op.toLong
  }


  def eval(input: Seq[String]): Long = {
    traverse(parseTree(Parser.Expr, input))
  }

  def parseTree(parser: Parser[ParseTree], input: Seq[String]): ParseTree = {
    val result: ParseResult[ParseTree] = parser.parse(input.reduce(_ + _)).done
    result.either match {
      case Left(e) => println(e)
        throw new RuntimeException(e)
      case Right(r) => r
    }
  }

  def evalArithPrecedence(input: Seq[String]): Long =
    traverse(parseTree(ParserArithPrecidenc.Expr, input))
}

object Parser {

  lazy val Expr: Parser[ParseTree] = for {
    lhs <- Factor
    next <- Expr_prim
  } yield (next(lhs))

  lazy val Expr_prim: Parser[ParseTree => ParseTree] = {
    val p1 = for {
      op <- char('+') | char('*')
      rhs <- Factor
      next <- Expr_prim
    } yield ((lhs: ParseTree) => (next(BinOp(op.toString, lhs, rhs))))

    val p2 = ok((lhs: ParseTree) => lhs)

    p1 | p2
  }

  lazy val Factor: Parser[ParseTree] = {
    val p1 = for {
      _ <- char('(')
      e <- Expr
      _ <- char(')')
    } yield e
    val p2 = for {
      s <- many(digit)
    } yield Terminal(s.foldLeft("")((acc, s) => acc + s))

    p1 | p2
  }
}

object ParserArithPrecidenc {
  lazy val Expr: Parser[ParseTree] = for {
    lhs <- Term
    next <- Expr_prime
  } yield next(lhs)

  lazy val Expr_prime: Parser[ParseTree => ParseTree] = {
    val p1 = for {
      op <- char('*')
      rhs <- Term
      next <- Expr_prime
    } yield ((lhs: ParseTree) => (next(BinOp(op.toString, lhs, rhs))))

    val p2 = ok((lhs: ParseTree) => lhs)

    p1 | p2
  }

  lazy val Term: Parser[ParseTree] = for {
    lhs <- Factor
    next <- Term_prime
  } yield next(lhs)

  lazy val Term_prime: Parser[ParseTree => ParseTree] = {
    val p1 = for {
      op <- char('+')
      rhs <- Factor
      next <- Term_prime
    } yield (lhs: ParseTree) => next(BinOp(op.toString, lhs, rhs))

    val p2 = ok((lhs: ParseTree) => lhs)
    p1 | p2
  }

  lazy val Factor: Parser[ParseTree] = {
    val p1 = for {
      _ <- char('(')
      e <- Expr
      _ <- char(')')
    } yield e
    val p2 = for {
      s <- many(digit)
    } yield Terminal(s.foldLeft("")((acc, s) => acc + s))

    p1 | p2
  }
}

