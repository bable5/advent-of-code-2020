package com.gitlab.bable5.aoc2020.day18

import com.gitlab.bable5.aoc2020.ResourceFileReader

object Day18Data extends ResourceFileReader {
  def apply(): Seq[Seq[String]] = readResource("day18.input")
    .toSeq
    .map(_.trim())
    .map(_.split("\\s"))
}
