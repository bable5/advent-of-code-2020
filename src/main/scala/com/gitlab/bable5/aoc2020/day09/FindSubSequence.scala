package com.gitlab.bable5.aoc2020.day09

import scala.annotation.tailrec

object FindSubSequence {

  @tailrec
  private def searchOneGroupSize(target: Long, groupSize: Int, input: Seq[Long]): Option[Seq[Long]] = {
    if (groupSize > input.size) None
    else {
      val subset = input.take(groupSize)
      if (subset.sum == target) {
        Option(subset)
      } else {
        searchOneGroupSize(target, groupSize, input.tail)
      }
    }
  }

  @tailrec
  private def subSequenceSearch(target: Long, groupSize: Int, input: Seq[Long]): Seq[Long] = {
    val subset = searchOneGroupSize(target, groupSize, input)
    if (subset.isDefined) {
      subset.get
    } else if(groupSize > input.size) {
      throw new IllegalArgumentException(s"Could not find a subsequence that sums to $target")
    } else {
      subSequenceSearch(target, groupSize + 1, input)
    }
  }

  def apply(target: Long, input: Seq[Long]): Seq[Long] = subSequenceSearch(target, 3, input)
}
