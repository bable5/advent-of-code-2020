package com.gitlab.bable5.aoc2020.day09

import com.gitlab.bable5.aoc2020.ResourceFileReader

object EncodingData extends ResourceFileReader {

  def apply(): Seq[Long] = readResource("day09.input")
    .toSeq
    .map(_.toLong)
}
