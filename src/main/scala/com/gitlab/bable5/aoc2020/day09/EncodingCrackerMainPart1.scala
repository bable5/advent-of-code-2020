package com.gitlab.bable5.aoc2020.day09

import com.gitlab.bable5.aoc2020.Timer.timed

object EncodingCrackerMainPart1 extends App {
  println(timed {
    EncodingCracker(25, EncodingData())
  })
}
