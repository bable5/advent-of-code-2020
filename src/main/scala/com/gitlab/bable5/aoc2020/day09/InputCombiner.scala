package com.gitlab.bable5.aoc2020.day09

object InputCombiner {

  def apply[A](vs: Iterable[A]): Iterable[(A, A)] = for {
    (x, xIdx) <- vs.zipWithIndex
    (y, yIdx) <- vs.zipWithIndex
    if xIdx < yIdx
  } yield (x, y)
}
