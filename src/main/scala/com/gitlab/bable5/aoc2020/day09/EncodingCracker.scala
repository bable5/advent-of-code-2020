package com.gitlab.bable5.aoc2020.day09

import scala.annotation.tailrec

object EncodingCracker {
  def partition[A](size: Int, xs: Seq[A]): (Seq[A], Seq[A]) = (
    xs.take(size), xs.drop(size)
  )

  def somePairSumsTo(target: Long, xs: Seq[Long]): Boolean =
    InputCombiner(xs).exists(p => p._1 + p._2 == target)

  def apply(partitionSize: Int, input: Seq[Long]): Long = {
    @tailrec
    def h(window: (Seq[Long], Seq[Long])): Long = {
      val target = window._2.head
      if (somePairSumsTo(target, window._1)) {
        val nextList = window._1.tail ++ window._2
        h(partition(partitionSize, nextList))
      } else {
        target
      }
    }

    h(partition(partitionSize, input))
  }
}
