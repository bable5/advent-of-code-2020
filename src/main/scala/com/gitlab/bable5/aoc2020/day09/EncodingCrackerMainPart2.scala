package com.gitlab.bable5.aoc2020.day09

import com.gitlab.bable5.aoc2020.Timer.timed

object EncodingCrackerMainPart2 extends App {
  println(timed {
    val input = EncodingData()
    val unmatchedValue = EncodingCracker(25, input)
    val subset = FindSubSequence(unmatchedValue, input).sorted
    subset.head + subset.last
  })
}
