package com.gitlab.bable5.aoc2020

object Timer {
  def timed[A](f: => A) = {
    val start = System.currentTimeMillis()
    val v = f
    val end = System.currentTimeMillis()
    println(s"Took ${end - start}ms")
    v
  }
}
