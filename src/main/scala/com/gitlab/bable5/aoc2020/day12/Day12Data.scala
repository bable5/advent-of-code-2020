package com.gitlab.bable5.aoc2020.day12

import com.gitlab.bable5.aoc2020.ResourceFileReader

object Day12Data extends ResourceFileReader {
  def apply(): Seq[Move] = readResource("day12.input")
    .toSeq
    .map(_.trim)
    .map(s => parseMovement(s))
}
