package com.gitlab.bable5.aoc2020.day12

object Day12Main extends App {

  val (x, y, h) = Day12Data().foldLeft((0, 0, 90))(nextPosition)
  println(x, y, h)
  println(Math.abs(x) + Math.abs(y))

}
