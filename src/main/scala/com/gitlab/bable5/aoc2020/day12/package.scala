package com.gitlab.bable5.aoc2020

package object day12 {

  sealed trait Direction

  case object Forward extends Direction

  case object Right extends Direction

  case object Left extends Direction

  case object North extends Direction

  case object South extends Direction

  case object East extends Direction

  case object West extends Direction

  type Move = (Direction, Int)

  type Position = (Int, Int, Int)

  def nextPosition(current: Position, movement: Move): Position = {
    val (x, y, h) = current
    movement match {
      case (North, v) => (x, y + v, h)
      case (South, v) => (x, y - v, h)
      case (East, v) => (x + v, y, h)
      case (West, v) => (x - v, y, h)
      case (Right, v) => (x, y, h + v)
      case (Left, v) => (x, y, h - v)
      case (Forward, v) => {
        val dx: Int = (Math.sin(Math.toRadians(h)) * v).toInt
        val dy: Int = (Math.cos(Math.toRadians(h)) * v).toInt
        (x + dx, y + dy, h)
      }
    }
  }

  private val directionRegEx = raw"(\w)(\d+)".r

  def parseMovement(s: String): Move = s match {
    case directionRegEx(d, v) =>
      val direction: Direction = d match {
        case "F" => Forward
        case "N" => North
        case "S" => South
        case "E" => East
        case "W" => West
        case "L" => Left
        case "R" => Right
      }
      (direction, v.toInt)
  }
}