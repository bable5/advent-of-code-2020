package com.gitlab.bable5.aoc2020

package object day05 {

  case class Seat(row: Int, column: Int) {
    val seatId: Int = row * 8 + column
  }

  object SeatLocationParser {

    def apply(s: String): Seat = {
      assert(s.length == 10)

      val rowDesc = s.substring(0, 7).split("").map(bspToken).toSeq
      val columnDesc = s.substring(7).split("").map(bspToken).toSeq

      val row = BinarySpacePartition(rowDesc, BSPRange(0, 127)).min
      val column = BinarySpacePartition(columnDesc, BSPRange(0, 7)).min

      Seat(row, column)
    }

    private def bspToken(s: String): BSPDirection = s match {
      case d if d == "F" || d == "L" => LowerHalf
      case d if d == "B" || d == "R" => UpperHalf
    }
  }

  object HighestSeatId {
    def apply(vs: Seq[String]): Int =
      vs.map(v => SeatLocationParser(v).seatId)
        .max
  }

}

