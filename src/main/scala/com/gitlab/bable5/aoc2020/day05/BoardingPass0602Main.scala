package com.gitlab.bable5.aoc2020.day05

object BoardingPass0602Main extends App {
  val seatIds = BoardingPassesData().map(s => SeatLocationParser(s).seatId).sorted

  var prevSeatId = 0
  for(s <- seatIds) {
    val diff = s - prevSeatId
    if( diff == 2 ){
      println(s"$diff: $prevSeatId, $s")
    }
    prevSeatId = s
  }
}
