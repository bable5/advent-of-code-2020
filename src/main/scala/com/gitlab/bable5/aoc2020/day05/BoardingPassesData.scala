package com.gitlab.bable5.aoc2020.day05

import com.gitlab.bable5.aoc2020.ResourceFileReader

object BoardingPassesData extends ResourceFileReader {

  def apply(): Seq[String] = readResource("day05.input").toSeq
}
