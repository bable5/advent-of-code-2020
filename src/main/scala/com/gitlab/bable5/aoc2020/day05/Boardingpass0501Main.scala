package com.gitlab.bable5.aoc2020.day05

import com.gitlab.bable5.aoc2020.Timer.timed

object Boardingpass0501Main extends App {
  println(timed {
    HighestSeatId(BoardingPassesData())
  })
}
