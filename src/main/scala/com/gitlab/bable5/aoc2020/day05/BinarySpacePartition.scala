package com.gitlab.bable5.aoc2020.day05

sealed trait BSPDirection

case object LowerHalf extends BSPDirection

case object UpperHalf extends BSPDirection

case class BSPRange(min: Int, max: Int)

object BinarySpacePartition {

  private def log[A](lbl: String)(v: A) = {
//    println(s"$lbl: $v")
    v
  }

  def apply(directions: Seq[BSPDirection], range: BSPRange): BSPRange = directions.foldLeft(range) { (acc, dir) =>
    dir match {
      case LowerHalf => log("Lower")(BSPRange(acc.min, acc.min + split(acc)))
      case UpperHalf => log("Upper")(BSPRange(acc.min + split(acc) + 1, acc.max))
    }

  }

  private def split(range: BSPRange): Int = (range.max - range.min) / 2
}

