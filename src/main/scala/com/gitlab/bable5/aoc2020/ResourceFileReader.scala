package com.gitlab.bable5.aoc2020

import scala.io.Source

trait ResourceFileReader {
  def readResource(file: String): Iterator[String] = Source.fromResource(file).getLines
}
