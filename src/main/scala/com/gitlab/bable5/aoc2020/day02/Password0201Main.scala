package com.gitlab.bable5.aoc2020.day02

object Password0201Main extends App {
  val start = System.currentTimeMillis()

  println(PasswordData().count(_.isValid))

  println(s"Took ${System.currentTimeMillis() - start} ms")
}
