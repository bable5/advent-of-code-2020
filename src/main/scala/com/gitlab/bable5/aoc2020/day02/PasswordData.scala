package com.gitlab.bable5.aoc2020.day02

import com.gitlab.bable5.aoc2020.ResourceFileReader

object PasswordData extends ResourceFileReader {
  def apply(): Seq[PasswordCriteria] = {
    readResource("day02.input")
      .toSeq
      .filter(_.nonEmpty)
      .map(InputParser.apply)
  }
}
