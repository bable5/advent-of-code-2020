package com.gitlab.bable5.aoc2020

package object day02 {
  case class PasswordCriteria(min: Int, max: Int, required: Char, password: String) {
    def isValid: Boolean = {
      val matchCount = password.chars().filter(_==required).count()
      matchCount >= min && matchCount <= max
    }

    def isValid2: Boolean = {
      val position1Matches = password.charAt(min - 1) == required
      val positions2Matches = password.charAt(max - 1) == required

      position1Matches ^ positions2Matches
    }
  }

  object InputParser {
    private val inputMatcher = raw"(\d*)-(\d*) (\w): (\w*)".r
    /**
     * Parse a single input line from the puzzle into the data model.
     *
     * @param s a single password criteria entry
     * @return
     */
    def apply(s: String): PasswordCriteria = {
      s.trim match {
        case inputMatcher(min, max, required, password) => PasswordCriteria(min.toInt, max.toInt, required.charAt(0), password)
        case wtf@_ => throw new RuntimeException(wtf)
      }
    }
  }
}
