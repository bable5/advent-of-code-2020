package com.gitlab.bable5.aoc2020.day02

object Password0202Main extends App {
  val start = System.currentTimeMillis()

  println(PasswordData().count(_.isValid2))

  println(s"Took ${System.currentTimeMillis() - start} ms")
}
