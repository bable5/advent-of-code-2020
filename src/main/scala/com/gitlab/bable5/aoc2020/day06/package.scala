package com.gitlab.bable5.aoc2020

package object day06 {
  val passportDataGrouper = new InputGrouper[String, List[Set[String]]] {
    override def newGroup(b: String): Boolean = b.isEmpty

    override def lift(b: String): List[Set[String]] = {
      List(b.split("").toSet)
    }
  }
}
