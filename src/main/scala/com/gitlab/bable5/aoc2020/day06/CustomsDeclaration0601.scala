package com.gitlab.bable5.aoc2020.day06

import com.gitlab.bable5.aoc2020.Timer.timed

object CustomsDeclaration0601 extends App {
  println(timed {
    CustomsDeclarationData()
      .map(_.reduce(_ union _))
      .map(_.size)
      .sum
  })

}
