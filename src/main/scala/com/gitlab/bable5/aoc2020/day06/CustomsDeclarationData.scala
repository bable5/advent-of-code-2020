package com.gitlab.bable5.aoc2020.day06

import com.gitlab.bable5.aoc2020.ResourceFileReader

object CustomsDeclarationData extends ResourceFileReader {

  def apply(): Seq[List[Set[String]]] =
    passportDataGrouper(readResource("day06.input").toSeq)
}
