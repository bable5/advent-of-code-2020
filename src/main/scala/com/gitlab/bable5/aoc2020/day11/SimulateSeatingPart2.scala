package com.gitlab.bable5.aoc2020.day11

object SimulateSeatingPart2 extends SeatingSimulator {
  override val seatCountingStrategy: SeatCounter = NeighborsCounterPart2
  override val transitionRules: TransitionRules = TransitionRulesPart2
}
