package com.gitlab.bable5.aoc2020

package object day11 {
  sealed trait Seat
  case object Empty extends Seat {
    override def toString: String = "L"
  }
  case object Occupied extends Seat{
    override def toString: String = "#"
  }

  case object Floor extends Seat {
    override def toString: String = "."
  }

  type Position = (Int, Int)
  type SeatingChart[A] = IndexedSeq[IndexedSeq[A]]

  def matchSeat(c: Char): Seat = c match {
    case 'L' => Empty
    case '#' => Occupied
    case '.' => Floor
  }

  def parseRow(row: String): IndexedSeq[Seat] = row.toCharArray.map(matchSeat).toIndexedSeq
}
