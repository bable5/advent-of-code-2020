package com.gitlab.bable5.aoc2020.day11

import com.gitlab.bable5.aoc2020.day11.SimulateSeatingPart1.step

import scala.annotation.tailrec

trait SeatingSimulator  {
  val seatCountingStrategy: SeatCounter
  val transitionRules: TransitionRules

  private def computeNext(p: Position, input: SeatingChart[Seat]): Seat = {
    val current = input(p._1)(p._2)
    transitionRules(current, seatCountingStrategy(p, input))
  }

  def apply(input: SeatingChart[Seat]): SeatingChart[Seat] = step(input)

  def step(input: SeatingChart[Seat]): SeatingChart[Seat] =
    for (x <- input.indices)
      yield for (y <- input(x).indices)
        yield (computeNext((x, y), input))

  def runToFixpoint(input: SeatingChart[Seat]): SeatingChart[Seat] = {
    @tailrec
    def stepall(s: SeatingChart[Seat]): SeatingChart[Seat] = {
      val next = step(s)
      if (next == s) next else stepall(next)
    }

    stepall(input)
  }

  def totalOccupiedSeats(input: SeatingChart[Seat]): Int = {
    input.flatten.count(_ == Occupied)
  }
}
