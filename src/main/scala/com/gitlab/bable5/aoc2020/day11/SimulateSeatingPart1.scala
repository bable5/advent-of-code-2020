package com.gitlab.bable5.aoc2020.day11

object SimulateSeatingPart1 extends SeatingSimulator {

  override val seatCountingStrategy: SeatCounter = NeighborCounterPart1
  override val transitionRules: TransitionRules = TransitionRulesPart1

}
