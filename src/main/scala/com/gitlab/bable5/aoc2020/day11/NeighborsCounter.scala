package com.gitlab.bable5.aoc2020.day11

import scala.annotation.tailrec

trait SeatCounter {
  def apply(p: Position, seatingChart: SeatingChart[Seat]): Int
}

object NeighborCounterPart1 extends SeatCounter {
  override def apply(p: Position, seatingChart: SeatingChart[Seat]): Int = {
    def count(x: Int, y: Int): Int = if ((x, y) == p) {
      0
    } else {
      if (x < 0 || x >= seatingChart.length || y < 0 || y >= seatingChart(x).length) {
        0
      } else {
        val status = seatingChart(x)(y)
        if (status == Occupied) 1 else 0
      }
    }

    (for (
      x <- p._1 - 1 to p._1 + 1;
      y <- p._2 - 1 to p._2 + 1
    ) yield count(x, y)
      ).sum

  }
}

object NeighborsCounterPart2 extends SeatCounter {
  override def apply(p: Position, seatingChart: SeatingChart[Seat]): Int = {
    @tailrec
    def countDirection(direction: Position, currentPosition: Position): Int = {
      val next@(cx, cy) = nextPositionToCheck(direction, currentPosition)
      if (next == p) {
        0
      } else {
        if (outOfBounds(seatingChart, cx, cy)) {
          0
        } else {
          seatingChart(cx)(cy) match {
            case Floor => countDirection(direction, next)
            case Empty => 0
            case Occupied => 1
          }
        }
      }
    }

    (for (
      x <- -1 to 1;
      y <- -1 to 1
    ) yield countDirection((x, y), p)).sum
  }

  private def nextPositionToCheck(direction: (Int, Int), currentPosition: (Int, Int)) =
    (currentPosition._1 + direction._1, currentPosition._2 + direction._2)


  private def outOfBounds(seatingChart: SeatingChart[Seat], cx: Int, cy: Int) =
    cx < 0 || cx >= seatingChart.length || cy < 0 || cy >= seatingChart(cx).length
}
