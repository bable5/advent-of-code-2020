package com.gitlab.bable5.aoc2020.day11

import com.gitlab.bable5.aoc2020.Timer.timed
import com.gitlab.bable5.aoc2020.day11.SimulateSeatingPart1._

object SeatingPart1Main extends App {
  println(timed {
    totalOccupiedSeats(runToFixpoint(SeatingData()))
  })
}
