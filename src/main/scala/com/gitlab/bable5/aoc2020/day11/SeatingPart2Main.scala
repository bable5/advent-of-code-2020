package com.gitlab.bable5.aoc2020.day11

import com.gitlab.bable5.aoc2020.Timer.timed
import com.gitlab.bable5.aoc2020.day11.SimulateSeatingPart2._

object SeatingPart2Main extends App {
  println(timed {
    totalOccupiedSeats(runToFixpoint(SeatingData()))
  })
}
