package com.gitlab.bable5.aoc2020.day11

import com.gitlab.bable5.aoc2020.ResourceFileReader

object SeatingData extends ResourceFileReader{
  def apply(): SeatingChart[Seat] = readResource("day11.input")
    .toIndexedSeq
    .map(parseRow)

}
