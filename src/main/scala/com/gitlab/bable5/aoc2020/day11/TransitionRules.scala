package com.gitlab.bable5.aoc2020.day11

trait TransitionRules {
  def apply(currentState: Seat, neighborCount: Int): Seat
}

class OccupiedLimitTransitionRules(occupiedLimit: Int) extends TransitionRules {
  override def apply(currentState: Seat, neighborCount: Int): Seat = currentState match {
    case Empty if neighborCount == 0 => Occupied
    case Occupied if neighborCount >= occupiedLimit => Empty
    case s@_ => s
  }
}

object TransitionRulesPart1 extends TransitionRules {
  private val limit4 = new OccupiedLimitTransitionRules(4)

  override def apply(currentState: Seat, neighborCount: Int): Seat = limit4(currentState, neighborCount)
}

object TransitionRulesPart2 extends TransitionRules {
  private val limit4 = new OccupiedLimitTransitionRules(5)

  override def apply(currentState: Seat, neighborCount: Int): Seat = limit4(currentState, neighborCount)
}

