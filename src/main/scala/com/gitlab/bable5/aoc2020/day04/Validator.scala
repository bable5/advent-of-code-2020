package com.gitlab.bable5.aoc2020.day04

object Validator {
  val requiredFields = Set(
    "byr",
    "iyr",
    "eyr",
    "hgt",
    "hcl",
    "ecl",
    "pid"
  )

  def apply(entry: (String, String)): Boolean = isValid(entry)

  def apply(entry: Seq[(String, String)]): Boolean = entry.forall(isValid)

  def hasRequiredFields(input: Seq[(String, String)]): Boolean = {

    val passportFields = input.map(_._1).toSet

    Validator.requiredFields.subsetOf(passportFields)
  }

  private def isValid(entry: (String, String)) = validations.get(entry._1).exists(_ (entry._2))

  private val validations: Map[String, Validation] = Map(
    "byr" -> (inRange(1920, 2002)),
    "iyr" -> (inRange(2010, 2020)),
    "eyr" -> (inRange(2020, 2030)),
    "hgt" -> hgtValidator,
    "hcl" -> hclValidator,
    "ecl" -> eclValidator,
    "pid" -> pidValidator,
    "cid" -> cidValidator
  )

  type Validation = String => Boolean

  private def hgtValidator(s: String): Boolean = {
    def hgtUnitsValidator(suffix: String, v: Validation)(s: String): Boolean = if (s.endsWith(suffix)) v(s.stripSuffix(suffix)) else false

    val hgtCmValidation: Validation = hgtUnitsValidator("cm", inRange(150, 193))
    val hgtInValidation: Validation = hgtUnitsValidator("in", inRange(59, 79))

    hgtCmValidation(s) || hgtInValidation(s)
  }

  private def hclValidator: Validation = raw"#(\d|[a-f]){6}".r.matches

  private def eclValidator(s: String): Boolean = Set("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(s)

  private def pidValidator: Validation = raw"^\d{9}".r.matches

  private def cidValidator(s: String): Boolean = true

  private def inRange(min: Int, max: Int)(v: String): Boolean = inRange(v.toInt, min, max)

  private def inRange(v: Int, min: Int, max: Int): Boolean = min <= v && v <= max
}
