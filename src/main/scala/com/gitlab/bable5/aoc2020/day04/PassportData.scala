package com.gitlab.bable5.aoc2020.day04

import com.gitlab.bable5.aoc2020.ResourceFileReader

object PassportData extends ResourceFileReader with InputCollector {
  def apply(): Seq[Seq[(String, String)]] =
    collectPassportEntry(readResource("day04.input").toSeq)
      .map(parsePassportEntry)
}
