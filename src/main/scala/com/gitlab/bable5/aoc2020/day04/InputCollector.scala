package com.gitlab.bable5.aoc2020.day04

import com.gitlab.bable5.aoc2020.InputGrouper

trait InputCollector {

  private case class EntriesAcc(current: String = "", results: Seq[String] = Nil) {
    def appendCurrent(s: String) =
      if (current.isEmpty) copy(s) else copy(s"$current $s")

    def pushResult() =
      EntriesAcc(results = results :+ current)
  }

  private val grouper = new InputGrouper[String, List[String]] {
    override def newGroup(b: String): Boolean = b.isEmpty

    override def lift(b: String): List[String] = {
      println(s"Lifting $b")
      b.split(raw"\s").toList
    }
  }

  def collectPassportEntry(input: Iterable[String]): Seq[List[String]] = grouper(input)

  def parsePassportEntry(s: List[String]): Seq[(String, String)] =
    s.map(_.split(":"))
      .map(elems => (elems(0), elems(1)))
}
