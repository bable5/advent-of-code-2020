package com.gitlab.bable5.aoc2020.day04

import com.gitlab.bable5.aoc2020.Timer.timed

object Passport0402Main extends App {
  println(timed {
    PassportData()
      .filter(Validator.hasRequiredFields)
      .count(p => Validator(p))
  })
}
