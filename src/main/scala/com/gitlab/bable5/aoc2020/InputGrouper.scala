package com.gitlab.bable5.aoc2020

import cats.kernel.Monoid

abstract class InputGrouper[B, A: Monoid] {
  private val empty = Monoid[A].empty

  private case class EntriesAcc(current: A = empty, results: Seq[A] = Nil) {
    def appendCurrent(s: A) = {
      if (current == empty) copy(s) else copy(Monoid[A].combine(current, s))
    }

    def pushResult() =
      EntriesAcc(results = results :+ current)
  }

  def newGroup(b: B): Boolean

  def lift(b: B): A

  def apply(input: Iterable[B]): Seq[A] = {
    input.foldLeft(EntriesAcc()) { (acc, v) =>
      if (newGroup(v)) {
        acc.pushResult()
      } else {
        acc.appendCurrent(lift(v))
      }
    }.pushResult().results
  }
}
