package com.gitlab.bable5.aoc2020.day01

object Accounting0102Main extends App {
  val start = System.currentTimeMillis()

  private val maybeTuple: Option[(Long, Long, Long)] =
    new Accounting0101().find3(2020, Accounting0101Data())

  println(s"Took ${System.currentTimeMillis() - start} ms")


  if (maybeTuple.isDefined) {
    val t = maybeTuple.get
    println(t._1 * t._2 * t._3)
  } else {
    throw new IllegalArgumentException("Did not find a tuple!")
  }
}
