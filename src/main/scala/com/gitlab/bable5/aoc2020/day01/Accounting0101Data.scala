package com.gitlab.bable5.aoc2020.day01

import com.gitlab.bable5.aoc2020.ResourceFileReader

object Accounting0101Data extends ResourceFileReader {
  def apply(): Seq[Long] = {
    readResource("accounting0101.input")
      .toSeq
      .filter(_.nonEmpty)
      .map(_.toLong)

  }
}
