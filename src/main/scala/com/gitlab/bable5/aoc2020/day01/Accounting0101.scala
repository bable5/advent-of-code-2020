package com.gitlab.bable5.aoc2020.day01

import scala.annotation.tailrec

class Accounting0101 {

  @tailrec
  final def find(target: Long, values: Iterable[Long]): Option[(Long, Long)] = {
    def checkList(x: Long, xs: Iterable[Long]) = xs.find(x1 => x1 + x == target)

    values match {
      case x :: xs =>
        val c = checkList(x, xs)
        if (c.isDefined) Some(x, c.get) else find(target, xs)
      case _ => None
    }
  }

  @tailrec
  final def find3(target: Long, values: Iterable[Long]): Option[(Long, Long, Long)] = {
    values match {
      case x1 :: xs =>
        val c = find(target - x1, xs)
        if (c.isDefined) Some(x1, c.get._1, c.get._2) else find3(target, xs)
      case _ => None
    }

  }
}
